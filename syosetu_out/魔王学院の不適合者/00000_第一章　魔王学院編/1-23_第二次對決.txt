我和米夏爬著階梯，朝地城上層移動。

「追得上？」

米夏問道。
儘管是讓莎夏無法施展「飛行」的魔法，但倘若以相同速度行走，當然追不上，畢竟對方說不定會用跑的。

「沒問題。」

我稍微抬起腳，咚地踏響階梯平臺的地板。
嘎嘎！咚咚咚咚咚隆！位於地下的地城開始大幅搖晃，是甚至讓人難以站立的震動。

「抓住我。」
「⋯⋯嗯⋯⋯」

米夏抓住我的手，勉強熬過了震動。
持續了大約一分多鐘，搖晃才總算平息下來。

「可以放開嘍。」

米夏輕輕地放手。

「你做了什麼？」
「我稍微改變地形，製造死路，這樣就算不想也會追上。」

我們邁步前進，不久後爬完階梯，眼前出現了一個明亮空間──是來時經過的自然魔法陣的房間。

莎夏就在這裡。
她看似手足無措地站著不動，是因為怎樣也找不到來時曾經走過的通道吧。剛才我踏響地面的動作大幅改變了地形，使這裡成為死路。

「嗨，莎夏。」

我一出聲，她便抖了一下，回過頭來。
她緊握權杖警戒著。

「這也是你搞的鬼嗎？」

是指房間變成死路的事吧。

「你以為我會告訴背叛者嗎？」

莎夏的眼神變得凶狠，似乎因為看不出我的目的而戒備萬分。

「想要權杖就殺了我吧。」
「放心，是因為米夏說想跟你和好。」

莎夏瞠圓雙眼，隨即焦躁不已。

「你是笨蛋嗎？這麼快就忘了我剛才對你做了什麼？」

她所說的話相當尖銳。
然而米夏只是目不轉睛地回望著她。

「真讓人傻眼。你還真是個笨蛋人偶。阿諾斯，你也是，居然真的答應這種人偶的要求。聽好嘍，你相當中意的那孩子並不存在，既無生命也無根源，只是個明天就會消失的破爛人偶唷。」
「嗯，這我剛才聽過了，所以怎麼了嗎？」

或許是沒料到我會這麼說吧，莎夏一時語塞。

「⋯⋯這樣啊，已經說了嗎？不過是個人偶，行為還真像個活人呢。開始害怕自己會消失了嗎？」

莎夏指責似的說道。

「不是的。」
「哪裡不是？」
「我會消失是註定的，我不害怕。」

米夏淡然說道：

「但在那之前，我想和莎夏和好，就只是這樣。」

莎夏惡狠狠地瞪向米夏。

「我想知道你真正的想法。」
「什麼想法？」

很罕見地，米夏像是感到遲疑般戰戰兢兢地問道：

「⋯⋯⋯⋯莎夏⋯⋯⋯⋯討厭我嗎⋯⋯？」

莎夏沒有回答這個問題。
她朝我如此說道：

「喂，要再跟我對決一次嗎？」

我還以為她要說什麼呢。真是得不到教訓的女人。

「你打算怎麼對決？」
「我接下來會畫個魔法陣，要是你第一次看到那個魔法陣便有辦法施展魔法，就算你贏；倘若施展不了，就算我贏。」

要使用他人構築的魔法陣施展魔法是相當困難的事，況且要是不知道那是什麼魔法，還必須在第一次看到時就完全理解術式。一般而言，這會是對畫出魔法陣的一方壓倒性有利的對決吧。

只要對手不是我。

「可以嗎？採取對我這麼有利的條件，要我再多讓幾步也行哦。」
「沒問題，就算是你也絕不可能辦到。」

唔，也就是說她有自信嗎？有意思。

「你要賭什麼？」
「你贏的話，我就回答那孩子的問題。」
「要是你贏呢？」
「我要你聽我的命令，施展一個魔法。」

真是奇妙的條件呢。

「什麼魔法？」
「哎呀？不確認一下，你就怕得不敢跟我對決嗎？」

哦，這傢伙相當懂得該如何挑釁呢。
並非採取「絕對服從自己命令」這種廣範圍的條件，反倒特意侷限在使用一個魔法上，應該是想相對提高「契約」的約束力吧。

「契約」本來就不是能用半吊子的代價廢棄的魔法，換句話說，她相當警戒我的強大魔力。條件愈是單純有限，「契約」的約束力便愈強。

「無所謂，我就答應吧。」

莎夏滿意地露出微笑，施展「契約」的魔法。我在確認完內容後簽字。

「所以呢？那是怎樣的魔法陣？」
「我現在開始畫。」

莎夏轉身邁開步伐，停下腳步的位置正好是房間中心處。她輕輕闔眼，以雙手握住豎立的權杖。
魔力粒子湧現，在她腳邊浮現作為魔法陣原型的魔力圓。那個圓逐漸擴大，遍及整個房間。
看來是相當大規模的魔法陣，以莎夏本來的力量想必有點困難吧。不過權杖與「不死鳥法袍」提升了她的魔力，輔助她構築魔法陣。
魔力圓上浮現魔法文字，陸陸續續顯現魔法門。

───

經過十幾分鍾，莎夏仍在構築魔法陣，但我依舊看不出這是怎樣的魔法。

理由有兩點。
第一，這是我所不知道的魔法。
與神話時代的任何魔法都不相似，也就是在這兩千年內新開發的魔法。從莎夏自信滿滿的態度來看，這也許是她自己開發的魔法。
第二，這個魔法陣根本尚未完成，看來就連十分之一都還不到，如此一來選項就太多了，即使是我也不可能篩選出這是怎樣的魔法。

「你打算畫到什麼時候？」
「別擔心，會趕在明天零時之前完成的，在那孩子消失之前唷。」

考量魔法陣的構築速度，應該會在即將來到明天零時之際完成吧。
原來如此，大概是想奪走我時間的作戰吧。若要趕在米夏消失前施展魔法，或許就會因為心急而失敗──她是這樣想的吧。
抑或是還有其他企圖。

「哎呀？有點著急了嗎？」
「既然要向我挑戰，你就儘管做好萬全的準備吧。無論你要動什麼手腳都沒用。」
「真有自信呢。你等著瞧吧，這次的勝利將會屬於我。」

明明已經在小組對抗測驗展現如此龐大的實力差距了，莎夏究竟打哪來的自信啊？畢竟她也不是不知道我的實力。

「有意思。看在你這魯莽的勇氣上，在魔法陣完成前，我就閉上眼睛吧。」

我當場坐下，閉上魔眼。
我施展「〈Ruby〉魔力時鐘〈Rt〉Teru〈／Rt〉〈／Ruby〉」的魔法，顯示時間。

莎夏專心構築著魔法陣。這麼大的規模一旦稍有失誤，時間就會來不及，她的自尊也不容許自己這麼做吧。
莎夏以非常專注的集中力，毫無失誤地畫著魔法陣。

不久後日落西山，房間裡照進了月光。
米夏目不轉睛地望著姐姐的身影。
像是要將她拚命構築魔法陣的模樣深深印在眼中般，連眨眼都感到可惜地凝視著。

就這樣，時間一分一秒地流逝，「魔力時鐘」指向深夜的十一點四十五分。