是夢。

明確地知道是在夢裡。
這種景象時隔十四年再次出現，是現實中不可能出現的景象。

不管怎麼說，這是轉世時被女神召喚的白色房間。

「啪啪啪，恭喜恭喜！你的功績點超過一定值，干涉命運的資源增加了。敬請期待女神的慈悲！」

而且，被召喚到這個房間的話，一定是那個主人⋯⋯女神。

「即使隔了十四年也沒變啊」
「不管怎麼說，不變的是你哦。因為我的性格和說話方式都是配合你來演算演出的哦。是你把我變成這樣的。嘿嘿！」

恐怕，扮演認為對方最容易說話的人格吧。
為什麼，站在我面前會成為這樣的人格呢？

可以推測，但露骨地懷疑，如果洞察的話就能看出真實的想法。因為這種易懂的感覺讓我感到安心。

「干涉命運資源的上限增加了⋯⋯原來如此。迪雅、塔爾特、瑪哈。和這三個人的相遇太巧了。雖說是在尋找，但能和那麼合適的人才一個接一個地相遇，這是怎麼回事呢？和那三個人合得來的事情，你是不是又做了什麼？」
「啊，你注意到了嗎？是的，稍微有點操縱著命運線。被那三個人救了吧？因為和所有的人在一起了。喲，美男子！」
「⋯⋯感覺不太好啊。和三個人之間的羈絆被認為是很重要的」
「啊，這個有點不一樣。連感情和行動都束縛起來，這是很浪費資源的。說實話我做不到。我所做的，只是讓你和想要的人材相遇，把命運稍微轉換一下方向而已。只是見面就已經是極限了。見面以後的事我不知道。我很自豪。把那三個人拉攏起來純粹是你的實力。討厭，我也會被攻略的！？」

那句話對我來說是救贖。
如果三人的感情都是由女神設計的，那就太空虛了。

「這樣啊。⋯⋯那太好了」
「順便一提，按照原來的歷史，今天就是瑪哈的忌日。真是太好了。能夠突破所有人的忌日」
「我聽說三個人都應該在今天之前喪命。」
「是的，我是這麼說的。那個，我的阿卡西記錄放在哪裡了？啊，找到了」

女神故意從異空取出厚厚的書籍。

「本來歷史上最早死亡的是塔爾特，因減少人口被拋棄在冬季的山上，以托瓦哈迪為目標，途中遭遇寒冷和飢餓的折磨而死亡。這是最糟糕的死亡方式。然後，下一個死亡的是迪雅，維科內在戰爭中敗北，被預料到那個優秀的魔法才能，為了生下繼承人，被變態貴族購買，哇，真厲害啊。人類真是愚蠢，這樣做豈不是不能生孩子了？被弄壞後廢棄處理，太可憐了」

如果沒有我的存在，那兩個人就算變成那樣也不奇怪。
正因為知道那件事，所以才會很焦躁。

「最後瑪哈，因為很可愛，孤兒院的缺德院長把她賣給了蘿莉控貴族。但是，很堅強呢。順利地融入到情人之中，把蘿莉控貴族玩弄於股掌之間。終於，準備以蘿莉控貴族為後盾開自己的店，可是魔爪！在嫉妒發狂的正妻指引下，被盜賊綁架了⋯⋯啊，女神不能說這樣的東西，會毀了清純的角色。那麼，今天就預定要死了」
「三個人都是不與我相遇就注定要死去的命運，這不是偶然嗎？」

應該有什麼理由。
這個女神看起來雖然是這樣，但其行動都是有意義的。

「嗯，在擺弄命運的時候，除了個人的能力、才智之外，對之後未來的影響力越大，就越難擺弄吧。越是優秀的人越難擺弄。但是，因為早死而沒有未來的孩子們命運力很小，所以雖然他們的能力很強，但是命運操縱起來卻很輕鬆。性價比不錯吧」
「這是把我們當成棋子什麼的口吻。」
「我在想。也就是說，我自己是連棋子都沒有的舞台裝置呢。不過，正如我所說的那樣，能做的事真的很少呢」

確實很少。
要說這十四年做出來的事，那也只是把把本該死去的三個少女和我撮合在一起而已。

「所以，不是為了說這種話才召喚我的吧⋯⋯我也有想問的事情。關於這個世界不知道的事情太多了。如果真想拯救世界的話，就把情報交給我」

從魔族米娜的相遇中感受到的。

我對這個世界太無知了。
如果不知道規則的話，就不可能在遊戲中獲勝。

「哎呀，討厭。我不是故意刁難你。告訴你規則，必須使用命中注定的資源。那才是其他什麼都做不到的」
「但是，你卻流利地說出了三人的事嗎？」
「啊，那很好。因為，你不是自己注意到了嗎？」

明明宣稱是舞台裝置，卻毫無感情的眼神看著我。

確實是那樣啊。

與三個人的相遇是女神安排的，如果沒有和我相遇，也能想像出三個人早就死了。

「那麼，說出來吧。召喚到這裡也用了資源吧。如果你是裝置的話，就只能做有意義的事」
「正如你所說。上面判斷你迄今為止的功績是值得拯救世界的，之後再從別處追加命運。因此，能使用的資源增加了。所以，近期會有獎勵，請好好收下。我是為了說這個才召喚你的」
「⋯⋯說那個內容是因為要消耗資源，所以不能說。而且，特意在這裡使用資源來召喚，是不忠告的話就會被忽略的東西嗎？」

女神嫣然一笑。

好像是正確答案。

居然做到這種地步，似乎是相當大的獎勵。

「知道了。一定收下⋯⋯我也想拯救這個世界」

作為羅格・托瓦哈迪而活著，從心底愛著積累起來的東西。
而且，我不想失去愛我養育我的父母，喜歡我的迪雅，塔爾特，瑪哈，三個人。

「嗯，那麼請加油。因為，只有你能夠依靠了」
「如果因為說了那些話而消耗了資源，我是不會原諒你的。」
「沒關係的。可是，你不是知道的嗎？因為很引人注目啊，用前世的知識以輕鬆的心情做俺TUEEEE的笨蛋。但是，如果因此被盯上，然後被人類殺掉的話，我就不會照顧他們了。啊，資源太浪費了。嗯，因為你已經死了，所以可以轉到這裡來。但是，從女神的角度來說，投資的基本是分散投資，我認為這一點並不好」

正如女神所說，我知道有轉生者的存在。

做這種事的人很顯眼，很容易被歐露娜的情報網所吸引。
雖然也有直接見面提出合作，但不知為何都是些沒有協調性的人，因此被排斥。

並且，現在也掌握了全體人員的滅亡。
由女神轉生到這個世界的人規格很高。

但是，終究只是在人類的範圍內，稍微疏忽大意就喪命了。

「那麼，這是夢的終點了。起床後請和可愛的瑪哈一起吃早餐吧。那麼～，女神的神付結束了。啊，累了。今天的工作已經結束了，來去做SPA，之後邊看電視邊喝一杯吧」

於是，白色的房間開始歪斜。

女神的獎勵到底是什麼呢？
必須在一定程度上進行考察，建立假設。不然的話，肯定會被搶走的。