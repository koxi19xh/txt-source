ついに明らかになる、驚愕の事實！


════════════════════

竜、是最強的存在。

是超越生物的存在。也就是說有著超越精靈和神的力量。
是由世界本身產生的、消滅眾神和支配著人類、破壊世界的存在。
但也有守護著世界的存在。

本來人類和魔族是沒有交集的、只是各活各的而已。而竜並不是。
偶爾會在歷史上出現、是把人類國家滅亡的困擾的存在。
但追根究底、其實人類那一方也有問題⋯⋯

雖然我這麼認為⋯可是。
真的有那麼強大的存在嗎、巴爾加斯懷疑著。

飛竜和地竜還有亞竜、戰鬥過好幾次了。然後現在、除了有經歷的亞竜以外、都有本事打倒。
再加上、這些值得信任的夥伴。１０年來併肩作戰的夥伴、和新加入的人、都非常可靠。
和這些夥伴一起對抗魔物、任何戰鬥都可以勝利的吧。
數年前在第一街區、還有有擊退一頭竜的實績。

───

而現在對手是年幼的竜。
體長１０公尺左右、和成年的地竜體積相比是相當小的。
因此輕視了。

「好～痛！」

巴爾加斯的大劍砍向竜時、遭到尾巴的反擊。
被彈飛落地而疼痛著。看來是太過輕視敵方了。
應該是被魔法技能極限強化的巴爾加斯、才一擊就被打飛了。
鎧甲發出不妙的聲音、看來骨頭碎了、內臓貌似也破裂了。

「好痛～！好痛～！」

暴躁的竜胡亂的攻擊著、比較靠近的戰士全被捲入而彈飛。
魔法的防禦等、為了消滅敵人而亂發脾氣般地動著。
而肉身的防禦力、就算被技能強化在牠面前也只是像紙片一樣。
到現在還沒出現死者、單純只是運氣好吧。

夏爾、瑪露、希茲娜等魔法使們平安無事。然後是魯道夫。
如果成員沒有穿著金屬製的硬鎧、早就死了吧。

受到重傷的戰士們、立刻用魔法治癒。
傷勢回復後馬上站了起來、但巴爾加斯就辦法了。

「嗚～、好痛～」

是因為疼痛而胡亂了一陣子嗎、竜噗通的把肚子壓向地面。
最後好不容易、注意到了周圍的情況、用有些困擾的聲音問著。

「抱歉啊。沒事嗎？」

像是真的很擔心的語氣、治療中的魔法使們全都呆然了。
莉雅獨自一人興奮著。

「果然、我還是參戰比較好？」
「不要。很痛啊」

看見巴爾加斯用渾身的力量砍出的地方、那邊的鱗片碎了。但也僅僅如此而已。沒有傷到底下。

「雖然不小心做過頭了、但誰也沒有死掉吧？」

歪？再說的同時竜的頭歪向一邊。像充滿人性一樣。超萌的。明明很大隻卻像是小動物一樣。
在莉雅幫忙治療戰士的期間、偷偷窺視著這邊的樣子。

全員終於恢復了意識、總之先在竜前排成一列吧。
戰鬥貌似結束了。但也沒有贏。

「吶啊、竜桑。竜全是這樣強大嗎？」
「成竜和我（僕）差不多。普通的幼竜通常還要再弱一些」

是因為戰鬥結束了嗎、語調變的很親切。
再仔細一看、這隻竜真的相當可愛啊。圓圓的鼻子和眼睛。

「所以你和一般的竜不太一樣？」

對巴爾加斯的疑問、竜點了頭。

「我的父親是庫拉利斯醬、母親是巴魯斯醬、所以生來就比較強」

聽到了不可思議的消息。
庫拉利斯、應該是黃金竜庫拉利斯吧。巴魯斯的話、就是暗黑竜巴魯斯不會錯吧。
暗黑竜巴魯斯是母親。

嗯⋯母親？

這個違和感、在成員之間漂浮著、暫時無視了眼前這隻有著精英血脈的竜。
卡薩莉雅王國的始祖蕾特・阿娜亞是女的。這是毫無疑問的。肖像畫和雕刻上都是這樣的。
暗黑竜巴魯斯的妻子、產下的孩子成為第二代的王。不是傳說、而是歴史書上記載的。上面還留有本人的話。

「巴魯斯大人是我唯一的丈夫」

好好的保存著在官方的紀錄中。

───

莉雅像是要確認而詢問。

「請問稍微問一下可以嗎？」
「啊、是」

竜是在害怕使用著敬語的莉雅嗎。

「有其他的竜也叫巴魯斯的嗎？」
「竜一般都是沒有名字的。賦予名字這件事、是和人類交流時才會得到」

有點答非所問。

「誒兜、人稱暗黑竜巴魯斯的竜、是你的母親？」
「是啊」
「但是我們先祖大人的父親、也是叫暗黑竜巴魯斯誒」
「有甚麼奇怪的嗎？」

睜著大眼歪著頭的竜。盯著這邊、超可愛的啦。

「但是、暗黑竜巴魯斯從父親變成母親很奇怪呀」
「所以作為你們先祖大人的父親君、作為母親生了我呦」

理所當然的語氣、竜說著奇妙的話。
此時薩基突然頓悟了甚麼。

「那個啊、歐捏醬」

可能是年齡的影響、想像力也是最豐富的。有著跳躍般的思想。

「說不定竜啊、是具有兩種性別的生物？」

！！！大家全都發出了驚呼聲。

雙重性別。也就是、陰Ｏ和Ｏ道都有的型態。是男也是女。

「是醬子嗎！？」
「誒嘿、在說甚麼不太明白吶」

竜不太理解。

「所以作為你的爸爸和媽媽那時是男人啊、要生我時為了有黃金竜之血脈、所以成為女人」
「小丑魚嗎！」

這次是薩基吐槽了、對這個吐槽莉雅回憶著。
莉雅在前世的官方電視節目上有看過、而學到這方面的知識。
小丑魚會為了生孩子而隨著環境改變性別。確實是這樣子呢。

「啊×７！」

莉雅在理解後。
不由得大叫了起來。

「是嗎、原來是這樣嗎⋯」

噗通、膝蓋跪地。理解了自己為何會做為一位女性轉生。

為何到了１４歳還沒有生理現象、明白了。
外表看似女人、其實並不是女人。

「咕啊×３！」

抱著頭站了起來。團員早就習慣了莉雅無法理解的行為、所以才能到現在還沒什麼吃驚。

咻、莉雅突然轉向竜。表情非常認真。

「那個、難道竜在平常、全員都是女的嗎？」
「是這樣的阿⋯啊、難道人類不是嗎？誒？不一樣嗎？」

竜桑、全員都是雌的。

「該、該怎樣變成男的！？」
「誒、不知道。我還沒辦法繁殖」

噗通、莉雅再次跪地。但是答案就在附近。

弗弗弗、露出了笑聲。

「能行！再撐一下！加油啊我！」

充滿氣勢的站起來。對這個亂七八糟的反應、周圍沒有人可以跟上。

「還可以問暗黑竜巴魯斯！來吧！把門打開吧！」
「啊、嗚嗯」

雖然被氣勢嚇到、竜還是詠唱了咒文。

巨大的門、終於在莉雅前打開。


════════════════════

やあ、やっと第１話からの謎が解明されましたね。
もちろんドラゴンちゃん、人間化出來ますよ。じゃないと子作りできませんし。
ちなみに殘りの神竜ですが水竜（青竜）は拉娜、火竜（赤竜）は奧瑪、風竜（白竜）は瑟魯という名前です。巴魯斯と合わせると元ネタ分かりやすいですね。